/*  ---------------------------------------------------
    Template Name: Ogani
    Description:  Ogani eCommerce  HTML Template
    Author: Colorlib
    Author URI: https://colorlib.com
    Version: 1.0
    Created: Colorlib
---------------------------------------------------------  */

'use strict';

(function ($) {

    /*------------------
        Preloader
    --------------------*/
    $(window).on('load', function () {
        $(".loader").fadeOut();
        $("#preloder").delay(200).fadeOut("slow");

        /*------------------
            Gallery filter
        --------------------*/
        $('.featured__controls li').on('click', function () {
            $('.featured__controls li').removeClass('active');
            $(this).addClass('active');
        });
        if ($('.featured__filter').length > 0) {
            var containerEl = document.querySelector('.featured__filter');
            var mixer = mixitup(containerEl);
        }
    });

    /*------------------
        Background Set
    --------------------*/
    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    //Humberger Menu
    $(".humberger__open").on('click', function () {
        $(".humberger__menu__wrapper").addClass("show__humberger__menu__wrapper");
        $(".humberger__menu__overlay").addClass("active");
        $("body").addClass("over_hid");
    });

    $(".humberger__menu__overlay").on('click', function () {
        $(".humberger__menu__wrapper").removeClass("show__humberger__menu__wrapper");
        $(".humberger__menu__overlay").removeClass("active");
        $("body").removeClass("over_hid");
    });

    /*------------------
		Navigation
	--------------------*/
    $(".mobile-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true
    });

    /*-----------------------
        Categories Slider
    ------------------------*/
    $(".categories__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 4,
        dots: false,
        nav: true,
        navText: ["<span class='fa fa-angle-left'><span/>", "<span class='fa fa-angle-right'><span/>"],
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        responsive: {

            0: {
                items: 1,
            },

            480: {
                items: 2,
            },

            768: {
                items: 3,
            },

            992: {
                items: 4,
            }
        }
    });


    $('.hero__categories__all').on('click', function(){
        $('.hero__categories ul').slideToggle(400);
    });

    /*--------------------------
        Latest Product Slider
    ----------------------------*/
    $(".latest-product__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        dots: false,
        nav: true,
        navText: ["<span class='fa fa-angle-left'><span/>", "<span class='fa fa-angle-right'><span/>"],
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true
    });

    /*-----------------------------
        Product Discount Slider
    -------------------------------*/
    $(".product__discount__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 3,
        dots: true,
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        responsive: {

            320: {
                items: 1,
            },

            480: {
                items: 2,
            },

            768: {
                items: 2,
            },

            992: {
                items: 3,
            }
        }
    });

    /*---------------------------------
        Product Details Pic Slider
    ----------------------------------*/
    $(".product__details__pic__slider").owlCarousel({
        loop: true,
        margin: 20,
        items: 4,
        dots: true,
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true
    });

    /*-----------------------
		Price Range Slider
	------------------------ */
    var rangeSlider = $(".price-range"),
        minamount = $("#minamount"),
        maxamount = $("#maxamount"),
        minPrice = rangeSlider.data('min'),
        maxPrice = rangeSlider.data('max');
    rangeSlider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            minamount.val('$' + ui.values[0]);
            maxamount.val('$' + ui.values[1]);
        }
    });
    minamount.val('$' + rangeSlider.slider("values", 0));
    maxamount.val('$' + rangeSlider.slider("values", 1));

    /*--------------------------
        Select
    ----------------------------*/
    $("select").niceSelect();

    /*------------------
		Single Product
	--------------------*/
    $('.product__details__pic__slider img').on('click', function () {

        var imgurl = $(this).data('imgbigurl');
        var bigImg = $('.product__details__pic__item--large').attr('src');
        if (imgurl != bigImg) {
            $('.product__details__pic__item--large').attr({
                src: imgurl
            });
        }
    });

    /*-------------------
		Quantity change
	--------------------- */
    var proQty = $('.pro-qty');
    proQty.prepend('<span class="dec qtybtn">-</span>');
    proQty.append('<span class="inc qtybtn">+</span>');
    proQty.on('click', '.qtybtn', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find('input').val(newVal);
    });

})(jQuery);

console.log("JOJO");
let carts = document.querySelectorAll('.add-cart');

let products = [
	{
		name: 'WaterMelon',
		tag: 'feature-4',
		price: 80,
		inCart: 0,
	},
	{
		name: 'Banana',
		tag: 'feature-2',
		price: 20,
		inCart: 0,
	},
	{
		name: 'Mpera',
		tag: 'feature-3',
		price: 10,
		inCart: 0,
	},
	{
		name:'Grapes',
		tag:'feature-5',
		price:40,
		inCart:0,
	},
]

for(let i = 0; i < carts.length; i++){
	carts[i].addEventListener('click',() =>{
		cartNumbers(products[i]);
		totalPrice(products[i])
	})
}

function onLoadCartNumbers(){
	let productNumbers = localStorage.getItem('cartNumbers');

	if(productNumbers){
		document.querySelector('.cart span').textContent = productNumbers;
		document.querySelector('.carts span').textContent = productNumbers;
		//document.querySelector('.carts2 span').textContent = productNumbers;

	}
}

function cartNumbers(product){
	let productNumbers = localStorage.getItem('cartNumbers');
	productNumbers = parseInt(productNumbers);

	if(productNumbers){
		localStorage.setItem('cartNumbers', productNumbers +1);
		document.querySelector('.cart span').textContent = productNumbers + 1;
		document.querySelector('.carts span').textContent = productNumbers + 1;
		document.querySelector('.carts2 span').textContent = productNumbers + 1;

	}
	else{
		localStorage.setItem('cartNumbers', 1);
		document.querySelector('.cart span').textContent = 1;
		document.querySelector('.carts span').textContent = 1;
		document.querySelector('.carts2 span').textContent = 1;

	}
	setItems(product);
}

function setItems(product){
	let cartItems = localStorage.getItem('productsInCart');
	cartItems = JSON.parse(cartItems);
	console.log(cartItems)

	if (cartItems != null){
		if (cartItems [product.tag] ==undefined){
			cartItems = {
				...cartItems,
				[product.tag]:product
			}
		}
		cartItems [product.tag].inCart +=1;

	}
	else{
		product.inCart = 1;
		cartItems = {
			[product.tag]:product
		}
	}

	localStorage.setItem("productsInCart", JSON.stringify(cartItems));
}

function totalPrice(product){
	let cartPrice = localStorage.getItem('totalPrice');
	console.log("total price is", cartPrice);

	if(cartPrice != null){
		cartPrice = parseInt(cartPrice);
		localStorage.setItem("totalPrice", cartPrice + product.price);
	}
	else{
		localStorage.setItem("totalPrice", product.price);
	}
}

function displayCart(){
	let cartItems = localStorage.getItem("productsInCart");
	console.log(cartItems);
	cartItems = JSON.parse(cartItems);

	let productContainer = document.querySelector(".products");

	if(cartItems && productContainer){
		productContainer.innerHTML = '';
		Object.values(cartItems).map(item =>{
			productContainer.innerHTML +=
			`
				<div class = "product">
					<img src ="./img/featured/${item.tag}.jpg" height="100" width="100" align-left>
					<span>${item.name}</span>
				</div>

				<div class"product-price">
					${item.price}
				</div>

				<div class"product-quantity">
					<span>${item.inCart}</span>
				</div>

				<div class"total-price">
					${item.inCart * item.price}
				</div>

			`;
		})

	}
}

onLoadCartNumbers();

displayCart();

(function() {
	'use strict';

	var data = [
	  { name: 'Bob', price: 40, quantity: '5',total: 200, },
	  { name: 'Elsie', price: 20, quantity: '1',total: 20, },
	  { name: 'Pomo', price: 80, quantity: '2',total: 160, },
	];

	var table, row, cell;

	table = document.getElementById('table');

	for (var i = 0; i < data.length; i++) {
	  // insert a new row at the end of the table
	  row = table.insertRow(-1);

	  for (var prop in data[i]) {
		cell = row.insertCell();
		cell.innerText = data[i][prop];
	  }
	}
  })();
